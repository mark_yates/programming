﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter fahrenheit or celsius for conversion");
            var temp = Console.ReadLine();

            switch (temp)
            {
                case "celsius":

                    Console.WriteLine("Please enter the temperature you have in celsius");
                    var c = int.Parse(Console.ReadLine());
                    var f = 1.8;
                    Console.WriteLine($"Here is the fahrenheit you desire { c * f + 32} ");
                    break;

                case "fahrenheit":

                    Console.WriteLine("Please enter the temperature you have in fahrenheit");
                    double e = double.Parse(Console.ReadLine());
                    Console.WriteLine($"Here is the celsius you desire {((e - 32) * 5) / 9}");
                    break;


                default:
                    Console.WriteLine("Invalid Input");
                    break;

            }
        }
    }
}
