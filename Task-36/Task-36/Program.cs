﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 10;

            for (var i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is {a}");

                if (i < 9)
                {
                    Console.WriteLine($"Not to ten yet...soon");
                }
                else
                {
                    Console.WriteLine("You made it to 10 well done!");
                }
            }
         
        }

        }

    }
    

