﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10
{
    class Program
    {
        static void Main(string[] args)
        {
            var f = 2036;
            var c = 2016;

            var a = f - c;

            Console.WriteLine($"Here are how many leap years there will be in the next 20 years {a / 4}");
        }
    }
}
