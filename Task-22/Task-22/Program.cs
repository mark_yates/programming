﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<string, string>();
            var f = new List<string>();
            var v = new List<string>();

            dict.Add("Grapefruit", "Fruit");
            dict.Add("Pumpkin", "Veg");
            dict.Add("Orange", "Fruit");
            dict.Add("Carrot", "Veg");



            foreach (var i in dict)
            {
                if (i.Value == "Fruit")
                {
                    f.Add(i.Key);
                }

                if(i.Value == "Veg")
                {
                    v.Add(i.Key);
                }

            }

            Console.WriteLine($"There is {f.Count} fruit and {v.Count} veg");
            f.ForEach(Console.WriteLine);
        }
    }
}
