﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please choose a number:");

            double n = double.Parse(Console.ReadLine());

            bool result = (n % 3 == 0) && (n % 4 == 0);

            Console.WriteLine("Your Number {0} is it divisible by 3 and 4? {1}", n, result);


        }
       }
      }
    