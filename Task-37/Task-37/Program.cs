﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {
            var hours = 10;
            var studyhours = 5;
            var weeks = 12;
            var credits = 15;

            var answer = hours * credits - studyhours * weeks;

            Console.WriteLine($"We need {hours} hours per credit earned");
            Console.WriteLine($"We need a total {credits} credits");
            Console.WriteLine($"$We get {5} hours of study polytech which comes off our total hours needed" );
            Console.WriteLine($"The course is {weeks} weeks long");
            Console.WriteLine($"The amount of study hours outside of polytech needed to pass this course is {answer} hours");
            Console.ReadLine();
        }
    }
}
