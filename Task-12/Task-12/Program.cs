﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number to see if it is even");
            var i = int.Parse(Console.ReadLine());

            bool result = i % 2 == 0;

            Console.WriteLine($"Is your number even {result}");
        }
    }
}
