﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_07
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c;

            Console.WriteLine("Please Enter a number you would like to multiply:");
            a = Convert.ToInt32(Console.ReadLine());

            for (b = 1; b <= 12; b++)

            {
                c = a * b;
                Console.WriteLine("{0}*{1}={2}", a, b, c);

            }
        }
    }
}
