﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<string, int>();
            var m = new List<string>();

            dict.Add("Jan", 31);
            dict.Add("Feb", 28);
            dict.Add("Mar", 31);
            dict.Add("Apr", 30);
            dict.Add("May", 31);
            dict.Add("Jun", 30);
            dict.Add("Jul", 31);
            dict.Add("Aug", 31);
            dict.Add("Sep", 30);
            dict.Add("Oct", 31);
            dict.Add("Nov", 30);
            dict.Add("Dec", 31);


            foreach (var i in dict)
            {
                if(i.Value == 31)
                {
                    m.Add(i.Key);
                }

            }

            Console.WriteLine($"In total there is {m.Count}");
            m.ForEach(Console.WriteLine) ;



        }
    }
}
