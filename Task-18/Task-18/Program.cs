﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            Tuple<string, string, int>[] name =

{
                new Tuple<string, string, int>("steve", "march", 12),
                new Tuple<string, string, int>("bob", "april", 15),
                new Tuple<string, string, int>("john", "june", 24)};

            foreach (var n in name)
                Console.WriteLine(n.ToString());
        }
    }
    }

