﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "";
            var years = 0;

            Console.WriteLine("Please enter your name");
            name = Console.ReadLine();

            Console.WriteLine("Please enter your age");
            years = int.Parse(Console.ReadLine());

            Console.Clear();

            Console.WriteLine("your name is " + name + " you are " + years + " years old");

            Console.WriteLine("ok goodbye");
        }
    }
}
